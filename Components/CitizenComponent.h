/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : AI Fury Strategy Sample
Purpose : Base class for citizens of the land, it handles all types of citizens

--------------------------------------------------------------------------------- */

#pragma once

#include "Unit.h"

class CLandOwnerComponent;
struct SBuildingComponent;

struct SCitizenComponent : public IUnit
{
	enum TIMERS
	{
		Timer_Build_Tick = 0,
		Timer_Stucked,
		Timer_Attack,
		Timer_Mission
	};
public:
	SCitizenComponent() = default;
	SCitizenComponent::~SCitizenComponent();
	// IEntityComponent
	virtual void Initialize() override;
	virtual uint64 GetEventMask_Unit() const override;
	virtual void ProcessEvent_Unit(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<SCitizenComponent>& desc);
	// ~IEntityComponent
	void Update_Unit(float frameTime) override;
	void Timer_Unit(int timerId) override;
	//Citizen
	//Building
	bool BuildBuilding(SBuildingComponent *pBld);
	int GetMaxProductAmmount() { return carriedProductMaxAmmount; }
	//Helping
	void SetReservedBuilding(SBuildingComponent *pBld);
	SBuildingComponent *GetReservedBuilding() { return pReservedBuildingToGrabFrom; }
	void MoveToLocation(const Vec3 pos);
	void InitializeWarrior();
	void Attack(SCitizenComponent *pEnemy);
	bool IsWarrior() { return bIsWarrior; }
	bool IsAtMission() { return bIsAtMission; }
	void SetIsAtMission(bool set) { bIsAtMission = set; }
	//~Citizen
protected:
	CAICharacterComponent *pAIComponent = nullptr;
	bool bIsWorking = false;
	const int carriedProductMaxAmmount = 200;
	SBuildingComponent *pReservedBuildingToGrabFrom = nullptr;

	bool bIsStucked = false;
	bool bIsWarrior = false;
	bool bWarriorInitialized = false;
	bool bCanAttack = false;
	bool bIsAtMission = false;
};
