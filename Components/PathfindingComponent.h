#pragma once

#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/ResourceTypes.h>
#include <CrySchematyc/MathTypes.h>
#include <CrySchematyc/IObject.h>
#include <CrySchematyc/Utils/SharedString.h>
#include <CrySchematyc/Reflection/TypeDesc.h>
#include <CryAISystem/IAISystem.h>
#include <CryAISystem/IMovementSystem.h>
#include <CryAISystem/MovementRequest.h>

struct SPathfindingComponent
	: public IEntityComponent
	, private IMovementActorAdapter
	, public IAIPathAgent
{
	enum Timers
	{
		Pathfinding_Timer_CheckMovement = 500
	};
protected:
	class CPathObstacles final
		: public IPathObstacles
	{
		virtual bool IsPathIntersectingObstacles(const NavigationMeshID meshID, const Vec3& start, const Vec3& end, float radius) const final { return false; }
		virtual bool IsPointInsideObstacles(const Vec3& position) const final { return false; }
		virtual bool IsLineSegmentIntersectingObstaclesOrCloseToThem(const Lineseg& linesegToTest, float maxDistanceToConsiderClose) const final { return false; }
	};
	virtual void Initialize() final;
	virtual void ProcessEvent(SEntityEvent& event) final;
	virtual uint64 GetEventMask() const final;
public:
	virtual ~SPathfindingComponent()
	{
		Reset();
	}
	static void ReflectType(Schematyc::CTypeDesc<SPathfindingComponent>& desc)
	{
		desc.SetGUID("{4BE02A08-807E-401C-808F-BD431B49A5B6}"_cry_guid);
		desc.SetEditorCategory("AI System");
		desc.SetLabel("Pathfinder");
		desc.SetDescription("Exposes the ability to get path finding callbacks");
		//desc.SetIcon("icons:ObjectTypes/object.ico");
		desc.SetComponentFlags({ IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
		desc.AddMember(&SPathfindingComponent::m_maxAcceleration, 'maxa', "MaxAcceleration", "Maximum Acceleration", "Maximum possible physical acceleration", 10.f);
	}
	virtual void RequestMoveTo(const Vec3 &position)
	{
		CRY_ASSERT_MESSAGE(m_movementRequestId.id == 0, "RequestMoveTo can not be called while another request is being handled!");
		MovementRequest movementRequest;
		movementRequest.entityID = GetEntityId();
		movementRequest.destination = position;
		movementRequest.callback = functor(*this, &SPathfindingComponent::MovementRequestCallback);
		movementRequest.style.SetSpeed(MovementStyle::Walk);
		movementRequest.type = MovementRequest::Type::MoveTo;
		m_state = Movement::StillFinding;
		m_movementRequestId = gEnv->pAISystem->GetMovementSystem()->QueueRequest(movementRequest);
		m_currentRequestedPosition = position;
	}
	virtual void CancelCurrentRequest()
	{
		CRY_ASSERT(m_movementRequestId.id != 0);
		gEnv->pAISystem->GetMovementSystem()->CancelRequest(m_movementRequestId);
		m_movementRequestId = 0;
		if (m_pathFinderRequestId != 0)
		{
			gEnv->pAISystem->GetMNMPathfinder()->CancelPathRequest(m_pathFinderRequestId);
			m_pathFinderRequestId = 0;
		}
		m_currentRequestedPosition = ZERO;
	}
	bool IsProcessingRequest() const { return m_movementRequestId != 0; }
	virtual void SetMaxAcceleration(float maxAcceleration);
	float GetMaxAcceleration() const { return m_maxAcceleration; }
	virtual void SetMovementRecommendationCallback(std::function<void(const Vec3& recommendedVelocity)> callback) { m_movementRecommendationCallback = callback; }
	virtual void Restart() 
	{ 
		Initialize(); 
	}
protected:
	virtual void                  OnMovementPlanProduced() final {}
public:
	virtual NavigationAgentTypeID GetNavigationAgentTypeID() const final { return m_navigationAgentTypeId; }
protected:
	virtual Vec3                  GetPhysicsPosition() const final { return GetEntity()->GetWorldPos(); }
	virtual Vec3                  GetVelocity() const final
	{
		if (IPhysicalEntity* pPhysicalEntity = m_pEntity->GetPhysicalEntity())
		{
			pe_status_dynamics dynStatus;
			if (pPhysicalEntity->GetStatus(&dynStatus))
			{
				return dynStatus.v;
			}
		}

		return ZERO;
	}
public:
	virtual Vec3                  GetMoveDirection() const final { return GetVelocity().GetNormalized(); }
protected:
	virtual Vec3                  GetAnimationBodyDirection() const final { return m_requestedTargetBodyDirection; }
	virtual EActorTargetPhase     GetActorPhase() const final { return eATP_None; }
	virtual void                  SetMovementOutputValue(const PathFollowResult& result) final;
	virtual void                  SetBodyTargetDirection(const Vec3& direction) final { m_requestedTargetBodyDirection = direction; }
	virtual void                  ResetMovementContext() final {}
	virtual void                  ClearMovementState() final;
	virtual void                  ResetBodyTarget() final {}
	virtual void                  ResetActorTargetRequest() final {}
public:
	virtual bool                  IsMoving() const final { return !GetVelocity().IsZero(0.01f); }
protected:
	virtual void                  RequestExactPosition(const SAIActorTargetRequest* request, const bool lowerPrecision) final {}

	virtual bool                  IsClosestToUseTheSmartObject(const OffMeshLink_SmartObject& smartObjectLink) const final { return false; }
	virtual bool                  PrepareNavigateSmartObject(CSmartObject* pSmartObject, OffMeshLink_SmartObject* pSmartObjectLink) final { return false; }
	virtual void                  InvalidateSmartObjectLink(CSmartObject* pSmartObject, OffMeshLink_SmartObject* pSmartObjectLink) final {}

	virtual void                  SetInCover(const bool inCover) final {}
	virtual void                  UpdateCoverLocations() final {}
	virtual void                  InstallInLowCover(const bool inCover) final {}
	virtual void                  SetupCoverInformation() final {}
	virtual bool                  IsInCover() const final { return false; }

	virtual bool                  GetDesignedPath(SShape& pathShape) const final { return false; }
	virtual void                  CancelRequestedPath() final {}
	virtual void                  ConfigurePathfollower(const MovementStyle& style) final {}

	virtual void                  SetActorPath(const MovementStyle& style, const INavPath& navPath) final {}
	virtual void                  SetActorStyle(const MovementStyle& style, const INavPath& navPath) final {}
	virtual void                  SetStance(const MovementStyle::Stance stance) final {}

	virtual std::shared_ptr<Vec3> CreateLookTarget() final { return nullptr; }
	virtual void                  SetLookTimeOffset(float lookTimeOffset) final {}
	virtual void UpdateLooking(float updateTime, std::shared_ptr<Vec3> lookTarget, const bool targetReachable, const float pathDistanceToEnd, const Vec3& followTargetPosition, const MovementStyle& style) final {}
	// ~IMovementActorAdapter

	// IAIPathAgent
	virtual IEntity *GetPathAgentEntity() const final { return GetEntity(); }
	virtual const char *GetPathAgentName() const final { return GetEntity()->GetClass()->GetName(); }

	virtual unsigned short GetPathAgentType() const final { return AIOBJECT_ACTOR; }

	virtual float GetPathAgentPassRadius() const final { return 1.f; }
	virtual Vec3 GetPathAgentPos() const final { return GetPathAgentEntity()->GetWorldPos(); }
	virtual Vec3 GetPathAgentVelocity() const final { return GetVelocity(); }

	virtual const AgentMovementAbility& GetPathAgentMovementAbility() const final { return m_movementAbility; }

	virtual void GetPathAgentNavigationBlockers(NavigationBlockers& blockers, const PathfindRequest* pRequest) final {}

	virtual unsigned int GetPathAgentLastNavNode() const final { return 0; }
	virtual void SetPathAgentLastNavNode(unsigned int lastNavNode) final {}

	virtual void SetPathToFollow(const char* pathName) final {}
	virtual void         SetPathAttributeToFollow(bool bSpline) final {}

	virtual void SetPFBlockerRadius(int blockerType, float radius) final {}

	virtual ETriState CanTargetPointBeReached(CTargetPointRequest& request) final
	{
		request.SetResult(eTS_false);
		return eTS_false;
	}

	virtual bool UseTargetPointRequest(const CTargetPointRequest& request) final { return false; }
public:
	virtual bool GetValidPositionNearby(const Vec3& proposedPosition, Vec3& adjustedPosition) const final;
	virtual bool IsLocationPossibleToReach(const Vec3 targetLocation);
	virtual Vec3 GetClosestValidNavigableLocation(const Vec3 targetLocation);
public:
	virtual bool GetTeleportPosition(Vec3& teleportPos) const final { return false; }

	virtual IPathFollower *GetPathFollower() const final { return m_pPathFollower.get(); }

	virtual bool IsPointValidForAgent(const Vec3& pos, uint32 flags) const final { return true;}
	// ~IAIPathAgent
public:
	void Reset()
	{
		gEnv->pAISystem->GetMovementSystem()->UnregisterEntity(GetEntityId());
		m_pPathFollower.reset();
		SAFE_RELEASE(m_pFoundPath);
	}
	Vec3 GetCurrentRequestPosition() { return m_currentRequestedPosition; }
protected:
	void MovementRequestCallback(const MovementRequestResult &result) {}

	INavPath *GetINavPath() { return m_pFoundPath; };
	Movement::PathfinderState GetPathfinderState() { return m_state; }
	void RequestPathTo(MNMPathRequest& request)
	{
		m_state = Movement::StillFinding;

		request.resultCallback = functor(*this, &SPathfindingComponent::OnMNMPathResult);
		request.agentTypeID = m_navigationAgentTypeId;

		m_pathFinderRequestId = gEnv->pAISystem->GetMNMPathfinder()->RequestPathTo(this, request);
	}

	void OnMNMPathResult(const MNM::QueuedPathID& requestId, MNMPathRequestResult& result)
	{
		m_pathFinderRequestId = 0;

		if (result.HasPathBeenFound())
		{
			m_state = Movement::FoundPath;

			SAFE_DELETE(m_pFoundPath);
			m_pFoundPath = result.pPath->Clone();

			// Bump version
			m_pFoundPath->SetVersion(m_pFoundPath->GetVersion() + 1);

			if (m_pPathFollower)
			{
				m_pPathFollower->Reset();
				m_pPathFollower->AttachToPath(m_pFoundPath);
			}
		}
		else
		{
			m_state = Movement::CouldNotFindPath;
		}
	}

	NavigationAgentTypeID m_navigationAgentTypeId;
	MovementActorCallbacks m_callbacks;

	MovementRequestID m_movementRequestId = 0;
	uint32 m_pathFinderRequestId;

	std::shared_ptr<IPathFollower> m_pPathFollower;
	CPathObstacles m_pathObstacles;

	Movement::PathfinderState m_state;
	INavPath *m_pFoundPath = nullptr;

	AgentMovementAbility m_movementAbility;

	Vec3 m_requestedTargetBodyDirection;

	Schematyc::Range<0, 10000> m_maxAcceleration = 6.0f;
	std::function<void(const Vec3& recommendedVelocity)> m_movementRecommendationCallback;

	Vec3 m_currentRequestedPosition = ZERO;

	bool m_bLastMovementState = false;
};