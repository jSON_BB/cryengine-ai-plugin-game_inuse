/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : AIFury Strategy Sample
Purpose : Storage building derives from building component, but it is special type of building

--------------------------------------------------------------------------------- */

#pragma once

#include "BuildingComponent.h"

class CStorageBuilding : public SBuildingComponent
{
public:
	CStorageBuilding() = default;
	CStorageBuilding::~CStorageBuilding() {}
	// IEntityComponent
	virtual void Initialize_Building() override;
	static void ReflectType(Schematyc::CTypeDesc<CStorageBuilding>& desc);
	// ~IEntityComponent
	//Building
	std::vector<int> GetStorage();
	void AddResource(int type, int ammount);
	void RemoveResource(int type, int ammount);
	void SetResource(int type, int ammount);
	int GiveProducts(int giveAmmount, int type = -1) override;
	int GetProductAmmount() override;
	float GetSupplyAmmount() override;
	int GetProductType() override;
	int GetSupplyType() override;
	bool IsSupplyFull() override { return false; }
	//~Building
protected:
	std::vector<int> storage;
	int startValues = 0;
};
