#include "Resource.h"
#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>

static void RegisterResource(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(SResource));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterResource)

void SResource::ReflectType(Schematyc::CTypeDesc<SResource>& desc)
{
	desc.SetGUID("{9C621BD5-31B4-4ABC-8FFE-59DBB0CF5F1F}"_cry_guid);
	desc.SetEditorCategory("Game");
	desc.SetLabel("Resource");
	desc.SetDescription("Simple component that defines resource");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
}
