#ifndef AI_CHARACTER_PROPERTIES_
#define AI_CHARACTER_PROPERTIES_
#include "PathfindingComponent.h"
#include "AdvancedAnimationComponent.h"
#include "CharacterControllerComponent.h"

struct SAnimationProperties
{
	inline bool operator==(const SAnimationProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SAnimationProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	Cry::DefaultComponents::EMeshType rType;
	Schematyc::CharacterFileName sCharacterFile;
	Cry::DefaultComponents::SRenderParameters pRenderParams;
	Schematyc::MannequinAnimationDatabasePath sDatabaseFile;
	SAdvancedAnimationComponent::SDefaultScopeSettings sDefaultScopeSettings;
	bool bAnimationDrivenMotion;
	bool bGroundAlignment;
	Cry::DefaultComponents::SPhysicsParameters pPhysicsParams;
};
static void ReflectType(Schematyc::CTypeDesc<SAnimationProperties>& desc)
{
	desc.SetGUID("{DB43F427-07A9-49FF-9520-E1B7F1E954B5}"_cry_guid);
	desc.AddMember(&SAnimationProperties::rType, 'type', "Type", "Type", "Determines the behavior of the static mesh", Cry::DefaultComponents::EMeshType::RenderAndCollider);
	desc.AddMember(&SAnimationProperties::sCharacterFile, 'file', "Character", "Character", "Determines the character to load", "Objects/Characters/SampleCharacter/thirdperson.cdf");
	desc.AddMember(&SAnimationProperties::sDatabaseFile, 'dbpa', "DatabasePath", "Animation Database", "Path to the Mannequin .adb file", "");
	desc.AddMember(&SAnimationProperties::sDefaultScopeSettings, 'defs', "DefaultScope", "Default Scope Context Name", "Default Mannequin scope settings", SAdvancedAnimationComponent::SDefaultScopeSettings());
	desc.AddMember(&SAnimationProperties::bAnimationDrivenMotion, 'andr', "AnimDriven", "Animation Driven Motion", "Whether or not to use root motion in the animations", false);
	desc.AddMember(&SAnimationProperties::bGroundAlignment, 'grou', "GroundAlign", "Use Ground Alignment", "Enables adjustment of leg positions to align to the ground surface", false);
}
//GAMEPLAY PROPERTIES

struct SGameplayProperties
{
	inline bool operator==(const SGameplayProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SGameplayProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	float fMoveSpeed = 20.5f;
};
static void ReflectType(Schematyc::CTypeDesc<SGameplayProperties>& desc)
{
	desc.SetGUID("{4AE7A7C1-E90E-4057-9471-BA6A8DD76DF2}"_cry_guid);
	desc.AddMember(&SGameplayProperties::fMoveSpeed, 'msp', "MoveSpeed", "Run speed", "AI movement speed", 20.5f);
}

struct SAIFuryCharacterProperties
{
	inline bool operator==(const SAIFuryCharacterProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SAIFuryCharacterProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }
	//CONTROLLER
	SPhysics pPhys;
	SMovement pMove;
	//ANIMATION PROPERTIES
	SAnimationProperties sAnimationProperties;
	//GAMEPLAY PROPERTIES
	SGameplayProperties sGameplayProperties;
};
static void ReflectType(Schematyc::CTypeDesc<SAIFuryCharacterProperties>& desc)
{
	desc.SetGUID("{5C126979-4BF6-4ABE-AF80-12BCC2756896}"_cry_guid);
	desc.SetLabel("AI Character settings");
	desc.SetDescription("AIFury character properties");
	//GAMEPLAY
	desc.AddMember(&SAIFuryCharacterProperties::sGameplayProperties, 'gmpl', "GameplayProperties", "Gameplay settings", "Player initial gameplay settings", SGameplayProperties());
	//PHYSICS
	desc.AddMember(&SAIFuryCharacterProperties::pPhys, 'pphy', "Physics", "Physical settings", "Player physics properties", SPhysics());
	//MOVEMENT
	desc.AddMember(&SAIFuryCharacterProperties::pMove, 'pmov', "Movement", "Movement settings", "Player movement properties", SMovement());
	//ANIMATIONS
	desc.AddMember(&SAIFuryCharacterProperties::sAnimationProperties, 'sani', "AnimationProperties", "Animation settings", "Setting of player animation system", SAnimationProperties());
}
#endif
