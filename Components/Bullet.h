#pragma once
#include "CryEntitySystem/IEntityComponent.h"
////////////////////////////////////////////////////////
// Physicalized bullet shot from weaponry, expires on collision with another object
////////////////////////////////////////////////////////
class CBulletComponent final : public IEntityComponent
{
public:
	virtual ~CBulletComponent() {}
	CBulletComponent(Vec3 headingDir);
	CBulletComponent() {}

	// IEntityComponent
	virtual void Initialize() override;
	static void ReflectType(Schematyc::CTypeDesc<CBulletComponent>& desc);

	virtual uint64 GetEventMask() const override { return BIT64(ENTITY_EVENT_COLLISION); }
	virtual void ProcessEvent(SEntityEvent& event) override;
	// ~IEntityComponent

private:
	const Vec3 headingDirection = ZERO;
};
