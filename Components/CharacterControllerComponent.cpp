#include "StdAfx.h"
#include "CharacterControllerComponent.h"
#include "CrySchematyc/ResourceTypes.h"
#include "CrySchematyc/IObject.h"
#include "CrySchematyc/Env/Elements/EnvFunction.h"

struct SCollisionSignal
{
	Schematyc::ExplicitEntityId otherEntityId;
	Schematyc::SurfaceTypeName surfaceType;
};

SCharacterControllerComponent::~SCharacterControllerComponent()
{
	SEntityPhysicalizeParams physParams;
	physParams.type = PE_NONE;
	m_pEntity->Physicalize(physParams);
}

void SCharacterControllerComponent::Initialize()
{
	Physicalize();

	if (m_bNetworked)
	{
		m_pEntity->GetNetEntity()->BindToNetwork();
	}
}

void SCharacterControllerComponent::ProcessEvent(SEntityEvent& event)
{
	if (event.event == ENTITY_EVENT_UPDATE)
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];

		IPhysicalEntity* pPhysicalEntity = m_pEntity->GetPhysics();
		CRY_ASSERT_MESSAGE(pPhysicalEntity != nullptr, "Physical entity removed without call to IEntity::UpdateComponentEventMask!");

		// Update stats
		pe_status_living livingStatus;
		if (pPhysicalEntity->GetStatus(&livingStatus) != 0)
		{
			m_bOnGround = !livingStatus.bFlying;

			// Store the ground normal in case it is needed
			// Note that users have to check if we're on ground before using, is considered invalid in air.
			m_groundNormal = livingStatus.groundSlope;
		}

		// Get the player's velocity from physics
		pe_status_dynamics playerDynamics;
		if (pPhysicalEntity->GetStatus(&playerDynamics) != 0)
		{
			m_velocity = playerDynamics.v;
		}
	}
	else if (event.event == ENTITY_EVENT_COLLISION)
	{
		// Collision info can be retrieved using the event pointer
		EventPhysCollision* physCollision = reinterpret_cast<EventPhysCollision*>(event.nParam[0]);

		const char* surfaceTypeName = "";
		EntityId otherEntityId = INVALID_ENTITYID;

		ISurfaceTypeManager* pSurfaceTypeManager = gEnv->p3DEngine->GetMaterialManager()->GetSurfaceTypeManager();
		if (ISurfaceType* pSurfaceType = pSurfaceTypeManager->GetSurfaceType(physCollision->idmat[1]))
		{
			surfaceTypeName = pSurfaceType->GetName();
		}

		if (IEntity* pOtherEntity = gEnv->pEntitySystem->GetEntityFromPhysics(physCollision->pEntity[1]))
		{
			otherEntityId = pOtherEntity->GetId();
		}

	}
	else if (event.event == ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED)
	{
		// Start validating inputs
#ifndef RELEASE
		// Slide value will have no effect if larger than the fall angle, since we'll fall instead
		if (m_movement.m_minSlideAngle > m_movement.m_minFallAngle)
		{
			m_movement.m_minSlideAngle = m_movement.m_minFallAngle;
		}
#endif

		m_pEntity->UpdateComponentEventMask(this);

		Physicalize();
	}
}

uint64 SCharacterControllerComponent::GetEventMask() const
{
	uint64 eventMask = BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);

	// Only update when we have a physical entity
	if (m_pEntity->GetPhysicalEntity() != nullptr)
	{
		eventMask |= BIT64(ENTITY_EVENT_UPDATE);
	}

	if (m_physics.m_bSendCollisionSignal)
	{
		eventMask |= BIT64(ENTITY_EVENT_COLLISION);
	}

	return eventMask;
}