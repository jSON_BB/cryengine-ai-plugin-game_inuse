#include "StdAfx.h"
#include "HealthComponent.h"
#include <CryEntitySystem/IEntity.h>
#include <CryEntitySystem/IEntitySystem.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>

static void RegisterHealthComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CHealthComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterHealthComponent);

void CHealthComponent::Initialize()
{
	bIsAlive = true;
	fValue = fMax;
}

uint64 CHealthComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_TIMER);
}

void CHealthComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];
		Update(pCtx->fFrameTime);
	}
	break;
	case ENTITY_EVENT_TIMER:
	{
		if (event.nParam[0] == 1)
		{
		}
	}
	break;
	};
}

void CHealthComponent::ReflectType(Schematyc::CTypeDesc<CHealthComponent>& desc)
{
	desc.SetGUID("{4C7312B7-6EB4-4010-8492-6FC0B3E70D80}"_cry_guid);
	desc.SetEditorCategory("GameplayComponents");
	desc.SetLabel("Health Component");
	desc.SetDescription("Applies health to the enitty and introduces death concept");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	//editor properties
	desc.AddMember(&CHealthComponent::fMax, 'maxh', "MaxHealth", "Maximum health", "Maximum health for this component", 100.f);
	desc.AddMember(&CHealthComponent::fRegenerationRatio, 'rege', "RegenerationRatio", "Regeneration ratio", "Regeneratio speed ratio - 0 is no regeneration", 0.2f);
}

void CHealthComponent::Update(float frameTime)
{
	if (!bIsAlive)
		return;

	//Regeneration
	if (fValue < fMax)
		fValue += fRegenerationRatio;
	if (fValue > fMax)
		fValue = fMax;
}

void CHealthComponent::Damage(float damage)
{
	if (bIsAlive)
	{
		fValue -= damage;
		//Death handling
		if (fValue <= 0.f)
		{
			bIsAlive = false;
			fValue = 0.f;
			return;
		}
	}
}

void CHealthComponent::Reset()
{
	fValue = fMax;
	bIsAlive = true;
}
