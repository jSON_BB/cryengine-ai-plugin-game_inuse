#include "StdAfx.h"
#include "PathfindingComponent.h"

inline bool OverlapCapsule(const Lineseg& lineseg, float radius, EAICollisionEntities aiCollisionEntities)
{
	primitives::capsule capsulePrim;
	capsulePrim.center = 0.5f * (lineseg.start + lineseg.end);
	capsulePrim.axis = lineseg.end - lineseg.start;
	capsulePrim.hh = 0.5f * capsulePrim.axis.NormalizeSafe(Vec3Constants<float>::fVec3_OneZ);
	capsulePrim.r = radius;

	intersection_params ip;
	ip.bNoBorder = true;
	ip.bNoAreaContacts = true;
	ip.bNoIntersection = true;
	ip.bStopAtFirstTri = true;
	ip.bThreadSafe = true;

	IPhysicalWorld* pPhysics = gEnv->pPhysicalWorld;
	float d = pPhysics->PrimitiveWorldIntersection(capsulePrim.type, &capsulePrim, Vec3Constants<float>::fVec3_Zero,
		aiCollisionEntities, 0, 0, geom_colltype0, &ip);

	return (d != 0.0f);
}

inline bool CheckBodyPos(const Vec3& floorPos, EAICollisionEntities aiCollisionEntities)
{
	Vec3 segStart = floorPos + Vec3(0.f, 0.f, 0.6f) + Vec3(0, 0, 0.25f);
	Vec3 segEnd = floorPos + Vec3(0, 0, 1.8f - 0.25f);
	Lineseg torsoSeg(segStart, segEnd);
	return !OverlapCapsule(torsoSeg, 0.25f, aiCollisionEntities);
}

bool IntersectSweptSphere(Vec3* hitPos, float& hitDist, const Lineseg& lineseg, float radius, EAICollisionEntities aiCollisionEntities, IPhysicalEntity** pSkipEnts, int nSkipEnts, int geomFlagsAny)
{
	primitives::sphere spherePrim;
	spherePrim.center = lineseg.start;
	spherePrim.r = radius;

	Vec3 dir = lineseg.end - lineseg.start;

	geom_contact* pContact = 0;
	geom_contact** ppContact = hitPos ? &pContact : 0;
	int geomFlagsAll = 0;

	float d = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(spherePrim.type, &spherePrim, dir,
		aiCollisionEntities, ppContact,
		geomFlagsAll, geomFlagsAny, 0, 0, 0, pSkipEnts, nSkipEnts);

	if (d > 0.0f)
	{
		hitDist = d;
		if (pContact && hitPos)
			*hitPos = pContact->pt;
		return true;
	}
	else
	{
		return false;
	}
}

inline bool IntersectSegment(Vec3& hitPos, const Lineseg& lineseg, EAICollisionEntities aiCollisionEntities, int filter = rwi_ignore_noncolliding | rwi_stop_at_pierceable)
{
	ray_hit hit;

	const RayCastResult& result = gEnv->pAISystem->GetGlobalRaycaster()->Cast(RayCastRequest(lineseg.start, lineseg.end - lineseg.start,
		aiCollisionEntities, filter));
	if (!result || (result[0].dist < 0.0f))
		return false;

	hitPos = result[0].pt;
	return true;
}

inline bool GetFloorPos(Vec3& floorPos, const Vec3& pos, float upDist, float downDist, float radius, EAICollisionEntities aiCollisionEntities)
{
	// (MATT) This function doesn't have the behaviour you might expect. It only searches up by radius amount, and checks down by
	// upDist + downDist + rad. Might or might not be safe to fix, but this code is being superseded. {2009/07/01}
	FUNCTION_PROFILER(GetISystem(), PROFILE_AI);
	Vec3 delta = Vec3(0, 0, -downDist - upDist);
	Vec3 capStart = pos + Vec3(0, 0, upDist);
	const Vec3 vRad(0, 0, radius);

	if (IntersectSegment(floorPos, Lineseg(pos + vRad, pos - Vec3(0.0f, 0.0f, downDist)), aiCollisionEntities, rwi_stop_at_pierceable | (geom_colltype_player << rwi_colltype_bit)))
		return true;

	if (delta.z > 0.0f)
		return false;

	float hitDist;

	float numSteps = 1.f + downDist * 0.5f;
	float fStepNumInv = 1.f / numSteps;
	Lineseg seg;
	for (float fStep = 0.f; fStep < numSteps; fStep += 1.f)
	{
		float frac1 = fStep * fStepNumInv;
		float frac2 = (fStep + 1.f) * fStepNumInv;
		seg.start = capStart + frac1 * delta;
		seg.end = capStart + frac2 * delta;
		if (IntersectSweptSphere(0, hitDist, seg, radius, aiCollisionEntities, 0, 0, rwi_stop_at_pierceable | (geom_colltype_player << rwi_colltype_bit)))
		{
			floorPos.Set(pos.x, pos.y, seg.start.z - (radius + hitDist));
			return true;
		}
	}
	return false;
}

struct SOnMovementRecommendation
{
	SOnMovementRecommendation() {}
	SOnMovementRecommendation(const Vec3& velocity)
		: m_velocity(velocity) {}

	Vec3 m_velocity;
};

static void ReflectType(Schematyc::CTypeDesc<SOnMovementRecommendation>& desc)
{
	desc.SetGUID("{904FDB70-5108-4DCF-9D1B-A1E205B63750}"_cry_guid);
	desc.SetLabel("On Movement Recommendation");
	desc.AddMember(&SOnMovementRecommendation::m_velocity, 'vel', "Velocity", "Velocity", "The velocity proposed by the path finding solver", Vec3(0.0f));
}

void SPathfindingComponent::Initialize()
{
	Reset();

	m_navigationAgentTypeId = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");

	m_callbacks.queuePathRequestFunction = functor(*this, &SPathfindingComponent::RequestPathTo);
	m_callbacks.checkOnPathfinderStateFunction = functor(*this, &SPathfindingComponent::GetPathfinderState);
	m_callbacks.getPathFollowerFunction = functor(*this, &SPathfindingComponent::GetPathFollower);
	m_callbacks.getPathFunction = functor(*this, &SPathfindingComponent::GetINavPath);

	gEnv->pAISystem->GetMovementSystem()->RegisterEntity(GetEntityId(), m_callbacks, *this);

	if (m_pPathFollower == nullptr)
	{
		PathFollowerParams params;
		params.maxAccel = m_maxAcceleration;
		params.maxSpeed = params.maxAccel;
		params.minSpeed = 0.f;
		params.normalSpeed = params.maxSpeed;

		params.use2D = false;

		m_pPathFollower = gEnv->pAISystem->CreateAndReturnNewDefaultPathFollower(params, m_pathObstacles);
	}

	m_movementAbility.b3DMove = true;
}

void SPathfindingComponent::SetMaxAcceleration(float maxAcceleration)
{
	m_maxAcceleration = maxAcceleration;

	PathFollowerParams params;
	params.maxAccel = m_maxAcceleration;
	params.maxSpeed = params.maxAccel;
	params.minSpeed = 0.f;
	params.normalSpeed = params.maxSpeed;

	params.use2D = false;

	m_pPathFollower->SetParams(params);
}

void SPathfindingComponent::ProcessEvent(SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_START_GAME:
	{
		Initialize();
	}
	break;
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		PathFollowerParams params;
		params.maxAccel = m_maxAcceleration;
		params.maxSpeed = params.maxAccel;
		params.minSpeed = 0.f;
		params.normalSpeed = params.maxSpeed;

		params.use2D = false;

		m_pPathFollower->SetParams(params);
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (!IsMoving())
		{
			if (m_bLastMovementState)
			{
				m_bLastMovementState = false;
				m_pEntity->SetTimer(Pathfinding_Timer_CheckMovement, 1000);
			}
		}
		else
		{
			if (!m_bLastMovementState)
			{
				m_bLastMovementState = true;
				m_pEntity->KillTimer(Pathfinding_Timer_CheckMovement);
			}
		}
	}
	break;
	case ENTITY_EVENT_TIMER:
	{
		if (event.nParam[0] == Pathfinding_Timer_CheckMovement)
		{
			if (IsProcessingRequest())
				CancelCurrentRequest();

			m_currentRequestedPosition = ZERO;
		}
	}
	break;
	}
}

uint64 SPathfindingComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED) | BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_TIMER);
}

void SPathfindingComponent::SetMovementOutputValue(const PathFollowResult& result)
{
	if (Schematyc::IObject* pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SOnMovementRecommendation(result.velocityOut), GetGUID());
	}

	if (m_movementRecommendationCallback)
	{
		m_movementRecommendationCallback(result.velocityOut);
	}
}

void SPathfindingComponent::ClearMovementState()
{
	if (Schematyc::IObject* pSchematycObject = m_pEntity->GetSchematycObject())
	{
		pSchematycObject->ProcessSignal(SOnMovementRecommendation(ZERO), GetGUID());
	}

	if (m_movementRecommendationCallback)
	{
		m_movementRecommendationCallback(ZERO);
	}
}

bool SPathfindingComponent::GetValidPositionNearby(const Vec3 & proposedPosition, Vec3 & adjustedPosition) const
{
	adjustedPosition = proposedPosition;
	if (!GetFloorPos(adjustedPosition, proposedPosition, 1.0f, 2.0f, 10.f, AICE_ALL))
		return false;

	static float maxFloorDeviation = 1.0f;
	if (fabsf(adjustedPosition.z - proposedPosition.z) > maxFloorDeviation)
		return false;

	if (!CheckBodyPos(adjustedPosition, AICE_ALL))
		return false;

	const Vec3 pushUp(.0f, .0f, .2f);

	return gEnv->pAISystem->GetNavigationSystem()->IsLocationValidInNavigationMesh(GetNavigationAgentTypeID(), proposedPosition);
}

bool SPathfindingComponent::IsLocationPossibleToReach(const Vec3 targetLocation)
{
	NavigationAgentTypeID agentId = GetNavigationAgentTypeID();
	//check if point is navigable(if exists in nav mesh)	//then check if is reachable from current point
	const bool bNavigable = gEnv->pAISystem->GetNavigationSystem()->IsLocationValidInNavigationMesh(agentId, targetLocation) && gEnv->pAISystem->GetNavigationSystem()->IsPointReachableFromPosition(agentId, m_pEntity, m_pEntity->GetWorldPos(), targetLocation);

	return bNavigable;
}

Vec3 SPathfindingComponent::GetClosestValidNavigableLocation(const Vec3 targetLocation)
{
	Vec3 vClosesLocation = ZERO;
	gEnv->pAISystem->GetNavigationSystem()->GetClosestPointInNavigationMesh(GetNavigationAgentTypeID(), targetLocation, 100.f, 100.f, &vClosesLocation);
	return vClosesLocation;
}

