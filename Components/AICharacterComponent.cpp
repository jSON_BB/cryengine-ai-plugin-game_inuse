#include "StdAfx.h"
#include "AICharacterComponent.h"
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "SpawnPoint.h"
#include <CryRenderer/IRenderAuxGeom.h>
#include "CryAISystem/IAISystem.h"
#include "CryAISystem/INavigationSystem.h"
#include "CryAISystem/INavigation.h"

static void RegisterAICharacterComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CAICharacterComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterAICharacterComponent);

void CAICharacterComponent::Initialize()
{
	pController = m_pEntity->GetOrCreateComponentClass<SCharacterControllerComponent>();
	pController->SetTransformMatrix(Matrix34::Create(Vec3(1.f), IDENTITY, Vec3(0, 0, 1.f)));
	pAnimations = m_pEntity->GetOrCreateComponentClass<SAdvancedAnimationComponent>();

	pAnimations->SetCharacterFile(ai_properties.sAnimationProperties.sCharacterFile.value.c_str());
	pAnimations->SetMannequinAnimationDatabaseFile(ai_properties.sAnimationProperties.sDatabaseFile.value.c_str());
	pAnimations->SetControllerDefinitionFile(ai_properties.sAnimationProperties.sDefaultScopeSettings.m_controllerDefinitionPath);
	pAnimations->SetDefaultScopeContextName(ai_properties.sAnimationProperties.sDefaultScopeSettings.m_contextName);
	pAnimations->SetAnimationDrivenMotion(ai_properties.sAnimationProperties.bAnimationDrivenMotion);
	pAnimations->SetDefaultFragmentName(ai_properties.sAnimationProperties.sDefaultScopeSettings.m_fragmentName);
	pAnimations->LoadFromDisk();
	walkTagId = pAnimations->GetTagId("Walk");
	weaponTagId = pAnimations->GetTagId("HasWeapon");
	pAudio = GetEntity()->GetOrCreateComponentClass<SEntityAudioComponent>("Shot", "DryShot", "Reload");
	pPathfinder = m_pEntity->GetOrCreateComponentClass<SPathfindingComponent>();
	pPathfinder->SetMovementRecommendationCallback([this](const Vec3& recommendedVelocity)
	{
		SetVelocity(recommendedVelocity);
	});
	Revive();
}

uint64 CAICharacterComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_TIMER) | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED) | BIT64(ENTITY_EVENT_RESET);
}

void CAICharacterComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_START_GAME:
	{
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];
		bool bCanUpdate = pAnimations != nullptr && pController != nullptr;

		if (CHealthComponent *pHealth = m_pEntity->GetComponent<CHealthComponent>())
			bCanUpdate = pHealth->IsAlive();

		if (bCanUpdate)
		{
			UpdateAnimation(pCtx->fFrameTime);
			Update(pCtx->fFrameTime);
		}
	}
	break;
	case ENTITY_EVENT_TIMER:
	{
		if (event.nParam[0] == 100)
		{
			bIsWaiting = false;
		}
		//Kill_Timer((int)event.nParam[0]);
	}
	break;
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		if (ai_properties != ai_previousProperties)
		{
			ai_previousProperties = ai_properties;
			pController->SetUp(ai_properties.pPhys, ai_properties.pMove);
			pAnimations->SetType(ai_properties.sAnimationProperties.rType);
			pAnimations->SetCharacterFile(ai_properties.sAnimationProperties.sCharacterFile.value.c_str());
			pAnimations->SetMannequinAnimationDatabaseFile(ai_properties.sAnimationProperties.sDatabaseFile.value.c_str());
			pAnimations->SetControllerDefinitionFile(ai_properties.sAnimationProperties.sDefaultScopeSettings.m_controllerDefinitionPath);
			pAnimations->SetDefaultScopeContextName(ai_properties.sAnimationProperties.sDefaultScopeSettings.m_contextName);
			pAnimations->SetDefaultFragmentName(ai_properties.sAnimationProperties.sDefaultScopeSettings.m_fragmentName);
			pAnimations->SetAnimationDrivenMotion(ai_properties.sAnimationProperties.bAnimationDrivenMotion);
			pAnimations->EnableGroundAlignment(ai_properties.sAnimationProperties.bGroundAlignment);
			pAnimations->LoadFromDisk();
			walkTagId = pAnimations->GetTagId("Walk");
			weaponTagId = pAnimations->GetTagId("HasWeapon");
			pAnimations->ResetCharacter();
		}
	}
	break;
	case ENTITY_EVENT_RESET:
	{
		if (pPathfinder)
		{
			pPathfinder->Restart();
		}
	}
	break;
	}
}

void CAICharacterComponent::ReflectType(Schematyc::CTypeDesc<CAICharacterComponent>& desc)
{
	desc.SetGUID("{17CEC7CF-6FC6-42FB-B694-E82B2C91E1B1}"_cry_guid);
	desc.SetEditorCategory("AI");
	desc.SetLabel("AI Fury Character");
	desc.SetDescription("AI Fury character that is driven by AI Fury component");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CAICharacterComponent::ai_properties, 'pars', "AIProperties", "AI character settings", "Properties of ai class", SAIFuryCharacterProperties());
}

void CAICharacterComponent::Revive()
{
	Spawn();
	GetEntity()->Hide(false);
	GetEntity()->SetWorldTM(Matrix34::Create(Vec3(1, 1, 1), IDENTITY, GetEntity()->GetWorldPos()));
	pAnimations->ResetCharacter();
	pController->Physicalize();
}

void CAICharacterComponent::Spawn()
{
	//if (gEnv->IsEditor())
	//	return;

	//auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
	//pEntityIterator->MoveFirst();
	//while (!pEntityIterator->IsEnd())
	//{
	//	IEntity *pEntity = pEntityIterator->Next();
	//	if (auto* pSpawner = pEntity->GetComponent<CSpawnPointComponent>())
	//	{
	//		pSpawner->SpawnEntity(m_pEntity);
	//		break;
	//	}
	//}
}

void CAICharacterComponent::StartFire()
{
	Vec3 shootTargetPos = ZERO;

	ray_hit hit;
	Vec3 origin = m_pEntity->GetWorldPos() + Vec3(0.f, 0.f, 1.2f);
	Vec3 dir = m_pEntity->GetWorldRotation().GetColumn1();
	Vec3 hitPoint = (dir * 1000.f);
	static const unsigned int rayflags = rwi_stop_at_pierceable | rwi_colltype_any;
	if (gEnv->pPhysicalWorld->RayWorldIntersection(origin, hitPoint, ent_all, rayflags, &hit, 1, m_pEntity->GetPhysicalEntity()))
	{
		if (hit.pCollider)
		{
			IEntity *pCollider = gEnv->pEntitySystem->GetEntityFromPhysics(hit.pCollider);
			if (pCollider && pCollider != m_pEntity)
			{
				if (CPlayerComponent *player = pCollider->GetComponent<CPlayerComponent>())
				{
					shootTargetPos = pCollider->GetWorldPos() + Vec3(0.f, 0.f, 1.2f);
				}
			}
		}
	}
	//weaponInHands->Shoot(shootTargetPos);
}

void CAICharacterComponent::UpdateAnimation(float frameTime)
{
	ICharacterInstance *pCharacter = pAnimations->GetCharacter();
	if (!gEnv->IsEditing())
	{
		pAnimations->SetTagWithId(walkTagId, (pController->IsWalking() || pPathfinder->IsMoving()));
		pAnimations->SetTagWithId(weaponTagId, bHasWeapon);
	}

	Vec3 ctrl = pController->GetMoveDirection();
	Vec3 pth = pPathfinder->GetMoveDirection();
	//CryLogAlways("Ctrl/pth = %f/%f %f/%f %f/%f", ctrl.x, pth.x, ctrl.y, pth.y, ctrl.z, pth.z);

	if ((pController->IsWalking() || pPathfinder->IsMoving()))
	{
		Vec3 dir = ctrl + pth;
		if (lookDir != ZERO)
			dir = lookDir;

		Quat newRotation = Quat::CreateRotationVDir(dir);

		Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(newRotation));
		ypr.y = 0;
		ypr.z = 0;
		newRotation = Quat(CCamera::CreateOrientationYPR(ypr));

		m_pEntity->SetRotation(newRotation);
	}
	else
	{
		Vec3 dir = lookDir;
		if (dir != ZERO)
		{
			Quat newRotation = Quat::CreateRotationVDir(dir);

			Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(newRotation));
			ypr.y = 0;
			ypr.z = 0;
			newRotation = Quat(CCamera::CreateOrientationYPR(ypr));

			m_pEntity->SetRotation(newRotation);
		}
	}
}

void CAICharacterComponent::Update(float frameTime)
{
}

void CAICharacterComponent::SetVelocity(Vec3 velocity)
{
	UpdateAnimation(0.f);
	Vec3 newVel = (ai_properties.sGameplayProperties.fMoveSpeed * velocity) / 25.f;
	pController->SetVelocity(newVel);
}

void CAICharacterComponent::GoTo(Vec3 pos)
{
	if (pPathfinder->IsProcessingRequest())
		pPathfinder->CancelCurrentRequest();

	pPathfinder->RequestMoveTo(pos);
}

void CAICharacterComponent::Set_Timer(int timerId, int ms)
{
	timerSet.emplace(std::make_pair(timerId, true));
	m_pEntity->SetTimer(timerId, ms);
}

void CAICharacterComponent::Kill_Timer(int timerId)
{
	auto it = timerSet.find(timerId);
	if (it != timerSet.end())
		timerSet.erase(it);

	m_pEntity->KillTimer(timerId);
}

bool CAICharacterComponent::Get_TimerState(int timerId)
{
	auto it = timerSet.find(timerId);
	if (it != timerSet.end())
	{
		return it->second;
	}

	return false;
}

bool CAICharacterComponent::TimerExists(int timerId)
{
	auto it = timerSet.find(timerId);
	if (it != timerSet.end())
	{
		return true;
	}
	return false;
}

