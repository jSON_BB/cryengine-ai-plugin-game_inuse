#include "StdAfx.h"
#include "Unit.h"
#include "GlobalResources.h"
#include "HealthComponent.h"
#include "CryEntitySystem/IEntity.h"
#include "GamePlugin.h"

void IUnit::Initialize()
{
}

uint64 IUnit::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_TIMER) | GetEventMask_Unit();
}

void IUnit::ProcessEvent(SEntityEvent & event)
{
	ProcessEvent_Unit(event);
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];
		Update(pCtx->fFrameTime);
	}
	break;
	case ENTITY_EVENT_TIMER:
	{
		Timer_Unit((int)event.nParam[0]);
	}
	break;
	}
}

void IUnit::ReflectType(Schematyc::CTypeDesc<IUnit>& desc)
{
	desc.SetGUID("{8C9BB367-8F88-422E-934A-B0BEA14E6795}"_cry_guid);
}

void IUnit::Update(float frameTime)
{
	Update_Unit(frameTime);

	if (bIsSelected)
		ShowSelectedInfo();

	if (!pLandOwner)
	{
		for each(CLandOwnerComponent *lo in CGamePlugin::GetLandOwners())
		{
			if (lo->GetTeam() == unitProperties.team)
			{
				pLandOwner = lo;
				pLandOwner->AddUnit(this);
				break;
			}
		}
	}
}

void IUnit::Select()
{
	bIsSelected = true;
}

void IUnit::Deselect()
{
	bIsSelected = false;
}

void IUnit::ShowSelectedInfo()
{
	DrawCircleOnTerrain(m_pEntity->GetWorldPos(), highlightRadius, highlightCircleColor, 10.f, circlePts);
	Vec3 screenPos(ZERO);
	gEnv->pRenderer->ProjectToScreen(m_pEntity->GetWorldPos().x, m_pEntity->GetWorldPos().y, m_pEntity->GetWorldPos().z + 2.f, &screenPos.x, &screenPos.y, &screenPos.z);
	screenPos.x = screenPos.x * 0.01f * gEnv->pRenderer->GetWidth();
	screenPos.y = screenPos.y * 0.01f * gEnv->pRenderer->GetHeight();
	string label = GetName();
	//string label = m_pEntity->GetName();
	gEnv->pRenderer->GetIRenderAuxGeom()->Draw2dLabel(screenPos.x, screenPos.y, 1.5f, ColorF(1.f, 1.f, 1.f), true, label);
	if (pHealth)
	{
		char buffer[32];
		const string curHealth_str = itoa((int)pHealth->GetHealth(), buffer, 10);
		const string maxHealth_str = itoa((int)pHealth->GetMaxHealth(), buffer, 10);
		label = curHealth_str + " / " + maxHealth_str;
		gEnv->pRenderer->GetIRenderAuxGeom()->Draw2dLabel(screenPos.x, screenPos.y + 15.f, 1.5f, ColorF(1.f, 1.f, 1.f), true, label);
	}
}

void IUnit::SetTeam()
{
	if (pLandOwner) unitProperties.team = pLandOwner->GetTeam();
}
