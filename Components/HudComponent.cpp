#include "StdAfx.h"
#include "HudComponent.h"
#include "Player.h"

void CHudComponent::Initialize()
{
	InitializeHud();
}

void CHudComponent::ReflectType(Schematyc::CTypeDesc<CHudComponent>& desc)
{
	desc.SetGUID("{49CFC97C-3682-48CD-B01B-FD754F752C87}"_cry_guid);
}

void CHudComponent::OnUIEvent(IUIElement * pSender, const SUIEventDesc & event, const SUIArguments & args)
{
	const string eventName = event.sDisplayName;

	if (eventName == "Create_Building")
	{
		string buildingName;
		args.GetArg(0, buildingName);

		if (CPlayerComponent *pPlayer = m_pEntity->GetComponent<CPlayerComponent>())
		{
			pPlayer->CreateBuilding(buildingName);
		}
	}
	else if (eventName == "Create_Unit")
	{
		string unitName;
		args.GetArg(0, unitName);
		
		if (CPlayerComponent *pPlayer = m_pEntity->GetComponent<CPlayerComponent>())
		{
			pPlayer->CreateUnit(unitName);
		}
	}
}

void CHudComponent::InitializeHud()
{
	pMgr = gEnv->pFlashUI->GetUIActionManager();
	pHud = gEnv->pFlashUI->GetUIElement("Hud");
	pHud_Show = gEnv->pFlashUI->GetUIAction("hudshow");
	pHud_Hide = gEnv->pFlashUI->GetUIAction("hudhide");
}

void CHudComponent::ShowHud()
{
	if (pMgr && pHud_Show)
	{
		bShowing = true;
		pMgr->StartAction(pHud_Show, "Hud");
		if (pHud)
			pHud->AddEventListener(this, "Hud");
	}
}

void CHudComponent::HideHud()
{
	if (pMgr && pHud_Hide)
	{
		bShowing = false;
		pMgr->StartAction(pHud_Hide, "Hud");
		if (pHud)
			pHud->RemoveEventListener(this);
	}
}

void CHudComponent::Toggle()
{
	if (bShowing)
		HideHud();
	else
		ShowHud();
}

void CHudComponent::AddBuildingButton(int x_pos, int y_pos, string name, int cost, int time)
{
	if (pHud)
	{
		char bf[32];
		string cost_str = itoa(cost, bf, 10);
		string time_str = itoa(time, bf, 10);
		SUIArguments args;
		args.AddArgument<int>(x_pos);
		args.AddArgument<int>(y_pos);
		args.AddArgument<string>(name);
		args.AddArgument<string>(cost_str);
		args.AddArgument<string>(time_str);
		args.AddArgument<string>("Create_Building");

		pHud->CallFunction("AddBuildingButton", args);
	}
}

void CHudComponent::RemoveBuildingButton(string name)
{
	if (pHud)
	{
		SUIArguments args;
		args.AddArgument<string>(name);

		pHud->CallFunction("RemoveBuildingButton", args);
	}
}

void CHudComponent::AddTextField(string name, string text, int x_pos, int y_pos, int size)
{
	if (pHud)
	{
		SUIArguments args;
		args.AddArgument<string>(name);
		args.AddArgument<string>(text);
		args.AddArgument<int>(x_pos);
		args.AddArgument<int>(y_pos);
		args.AddArgument<int>(size);

		pHud->CallFunction("AddTextField", args);
	}
}

void CHudComponent::UpdateTextField(string name, string text, int x_pos, int y_pos, int size, bool visible)
{
	if (pHud)
	{
		SUIArguments args;
		args.AddArgument<string>(name);
		args.AddArgument<string>(text);
		args.AddArgument<int>(x_pos);
		args.AddArgument<int>(y_pos);
		args.AddArgument<int>(size);
		args.AddArgument<bool>(visible);

		pHud->CallFunction("UpdateTextField", args);
	}
}

void CHudComponent::AddUnitInfoPanel(string name)
{
	if (pHud)
	{
		SUIArguments args;
		args.AddArgument<string>(name);

		pHud->CallFunction("AddUnitInfoPanel", args);
	}
}

void CHudComponent::AddButtonToInfoPanel(string panelName, int x_pos, int y_pos, string name, int cost, int time)
{
	if (pHud)
	{
		char bf[32];
		string cost_str = itoa(cost, bf, 10);
		string time_str = itoa(time, bf, 10);
		SUIArguments args;
		args.AddArgument<string>(panelName);
		args.AddArgument<int>(x_pos);
		args.AddArgument<int>(y_pos);
		args.AddArgument<string>(name);
		args.AddArgument<string>(cost_str);
		args.AddArgument<string>(time_str);
		args.AddArgument<string>("Create_Unit");

		pHud->CallFunction("AddButtonToInfoPanel", args);
	}
}

void CHudComponent::SetVisibilityUnitInfo(string panelName, bool products, bool supplies, bool making)
{
	if (pHud)
	{
		SUIArguments args;
		args.AddArgument<string>(panelName);
		args.AddArgument<bool>(products);
		args.AddArgument<bool>(supplies);
		args.AddArgument<bool>(making);

		pHud->CallFunction("SetVisibilityUnitInfo", args);
	}
}

void CHudComponent::SelectUnitInfoPanel(string name)
{
	if (pHud)
	{
		SUIArguments args;
		args.AddArgument<string>(name);

		pHud->CallFunction("SelectUnitInfoPanel", args);
	}
}

void CHudComponent::UpdateUnitInfo(string products, string supplies, string health, string making)
{
	if (pHud)
	{
		SUIArguments args;
		args.AddArgument<string>(products);
		args.AddArgument<string>(supplies);
		args.AddArgument<string>(health);
		args.AddArgument<string>(making);

		pHud->CallFunction("UpdateUnitInfo", args);
	}
}

void CHudComponent::ShowUnitInfo(string name, bool show)
{
	if (pHud)
	{
		SUIArguments args;
		args.AddArgument<string>(name);
		args.AddArgument<bool>(show);

		pHud->CallFunction("ShowUnitInfo", args);

		if (show)
			SelectUnitInfoPanel(name);
	}
}
