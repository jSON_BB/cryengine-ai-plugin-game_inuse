/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : AIFury Strategy Sample
Purpose : Entire hud handling

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include <FlashUI/FlashUI.h>

class CHudComponent : public IEntityComponent, IUIElementEventListener
{
public:
	CHudComponent() = default;
	CHudComponent::~CHudComponent() {}
	// IEntityComponent
	virtual void Initialize() override;
	static void ReflectType(Schematyc::CTypeDesc<CHudComponent>& desc);
	// ~IEntityComponent
	//Event listener
	virtual void OnUIEvent(IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args) override;
	//~Event listener
	//HUD
	void InitializeHud();
	void ShowHud();
	void HideHud();
	void Toggle();
	void AddBuildingButton(int x_pos, int y_pos, string name, int cost, int time);
	void RemoveBuildingButton(string name);
	void AddTextField(string name, string text, int x_pos, int y_pos, int size);
	void UpdateTextField(string name, string text, int x_pos, int y_pos, int size, bool visible);
	//unit info
	void AddUnitInfoPanel(string name);
	void AddButtonToInfoPanel(string panelName, int x_pos, int y_pos, string name, int cost, int time);
	void SetVisibilityUnitInfo(string panelName, bool products, bool supplies, bool making);
	void SelectUnitInfoPanel(string name);
	void UpdateUnitInfo(string products, string supplies, string health, string making);
	void ShowUnitInfo(string name, bool show);
	//~HUD
private:
	bool bShowing = false;

	IUIElement *pHud;
	IUIAction *pHud_Show;
	IUIAction *pHud_Hide;
	IUIActionManager *pMgr;
};
