#pragma once
#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntity.h>
#include "FuryResources.h"

////////////////////////////////////////////////////////
// Spawn point
////////////////////////////////////////////////////////

enum class UnitType
{
	Worker = 0,
	Helper,
	Warrior
};
static void ReflectType(Schematyc::CTypeDesc<UnitType>& desc)
{
	desc.SetGUID("{F55BE218-BA9B-4487-91C6-89FB00149E6B}"_cry_guid);
	desc.SetLabel("UnitType");
	desc.SetDescription("Unit types");
	desc.AddConstant(UnitType::Worker, "Worker", "Worker");
	desc.AddConstant(UnitType::Helper, "Helper", "Helper");
	desc.AddConstant(UnitType::Warrior, "Warrior", "Warrior");
}

class CSpawnPointComponent final : public IEntityComponent
{
public:
	CSpawnPointComponent() = default;
	virtual ~CSpawnPointComponent() {}

	virtual void Initialize() override;

	// Reflect type to set a unique identifier for this component
	// and provide additional information to expose it in the sandbox
	static void ReflectType(Schematyc::CTypeDesc<CSpawnPointComponent>& desc);

public:
	void SpawnEntity(IEntity* otherEntity);

	int team;
	UnitType unitType;
};
