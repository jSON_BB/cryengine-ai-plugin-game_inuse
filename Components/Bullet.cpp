#include "StdAfx.h"
#include "Bullet.h"
#include "CryEntitySystem/IEntity.h"
#include "CryEntitySystem/IEntitySystem.h"
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "Player.h"
#include "AICharacterComponent.h"

static void RegisterBulletComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CBulletComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterBulletComponent);

CBulletComponent::CBulletComponent(Vec3 headingDir)
	: headingDirection(headingDir)
{
}

void CBulletComponent::Initialize()
{
	m_pEntity->LoadGeometry(0, "Objects/Bullets/SampleBullet.cgf");

	if (!gEnv->IsEditing())
	{
		pe_params_particle partParams;
		partParams.flags = particle_traceable | particle_no_roll | pef_log_collisions;
		partParams.mass = 0.0000016f;
		partParams.size = 0.00036f;
		partParams.thickness = 0.1f;
		partParams.gravity = Vec3(0, 0, 0);
		partParams.kAirResistance = 0.0f;
		partParams.surface_idx = 0;
		partParams.velocity = 360.f;
		if (headingDirection == ZERO)
			partParams.heading = m_pEntity->GetForwardDir();
		else
			partParams.heading = headingDirection;
		SEntityPhysicalizeParams params;
		params.pParticle = &partParams;
		params.type = PE_PARTICLE;
		m_pEntity->Physicalize(params);
	}

	m_pEntity->SetViewDistRatio(255);
}

void CBulletComponent::ReflectType(Schematyc::CTypeDesc<CBulletComponent>& desc)
{
	desc.SetGUID("{ED2AA3E5-2C57-423C-AF26-62D829ACE736}"_cry_guid);
	desc.SetEditorCategory("GameplayComponents/Bullets");
	desc.SetLabel("Bullet Component");
	desc.SetDescription("Example projectile bullet component");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
}

void CBulletComponent::ProcessEvent(SEntityEvent & event)
{
	// Handle the OnCollision event, in order to have the entity removed on collision
	if (event.event == ENTITY_EVENT_COLLISION)
	{
		EventPhysCollision *physCollision = reinterpret_cast<EventPhysCollision *>(event.nParam[0]);
		if (physCollision)
		{
			IPhysicalEntity *pThisEntityPhysics = physCollision->pEntity[0];
			IEntity *pThisEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pThisEntityPhysics);
			IPhysicalEntity *pColliderPhysics = physCollision->pEntity[1];
			IEntity *pCollider = gEnv->pEntitySystem->GetEntityFromPhysics(pColliderPhysics);
			//if collider is a valid entity
			if (pCollider)
			{
				if (CHealthComponent *pVictimHealth = pCollider->GetComponent<CHealthComponent>())
				{
					if (pVictimHealth->IsAlive())
					{
						pVictimHealth->Damage(30.f);

						pe_action_impulse impulse;
						impulse.impulse = GetEntity()->GetForwardDir() * 6.f;
						pCollider->GetPhysicalEntity()->Action(&impulse);
					}
				}
			}
			//in the end remove this entity from the stage
			gEnv->pEntitySystem->RemoveEntity(GetEntityId());
		}
	}
}