#include "StdAfx.h"
#include "AdvancedAnimationComponent.h"
#include <Cry3DEngine/IRenderNode.h>
#include "ICryMannequinUserParams.h"


static void ReflectType(Schematyc::CTypeDesc<EMotionParamID>& desc)
{
	desc.SetGUID("{AC89FDA5-CB1B-431F-BA71-FB4FFFDA16E2}"_cry_guid);
	desc.SetLabel("Animation Motion Parameter");
	desc.SetDescription("Modifier for animation to affect blend spaces");
	desc.SetDefaultValue(EMotionParamID::eMotionParamID_TravelSpeed);
	desc.AddConstant(EMotionParamID::eMotionParamID_TravelSpeed, "TravelSpeed", "Travel Speed");
	desc.AddConstant(EMotionParamID::eMotionParamID_TurnSpeed, "TurnSpeed", "Turn Speed");
	desc.AddConstant(EMotionParamID::eMotionParamID_TravelAngle, "TravelAngle", "Travel Angle");
	desc.AddConstant(EMotionParamID::eMotionParamID_TravelSlope, "TravelSlope", "Travel Slope");
	desc.AddConstant(EMotionParamID::eMotionParamID_TurnAngle, "TurnAngle", "Turn Angle");
	desc.AddConstant(EMotionParamID::eMotionParamID_TravelDist, "TravelDist", "Travel Distance");
	desc.AddConstant(EMotionParamID::eMotionParamID_StopLeg, "StopLeg", "Move2Idle Stop Leg");

	desc.AddConstant(EMotionParamID::eMotionParamID_BlendWeight, "BlendWeight", "Custom 1");
	desc.AddConstant(EMotionParamID::eMotionParamID_BlendWeight2, "BlendWeight2", "Custom 2");
	desc.AddConstant(EMotionParamID::eMotionParamID_BlendWeight3, "BlendWeight3", "Custom 3");
	desc.AddConstant(EMotionParamID::eMotionParamID_BlendWeight4, "BlendWeight4", "Custom 4");
	desc.AddConstant(EMotionParamID::eMotionParamID_BlendWeight5, "BlendWeight5", "Custom 5");
	desc.AddConstant(EMotionParamID::eMotionParamID_BlendWeight6, "BlendWeight6", "Custom 6");
	desc.AddConstant(EMotionParamID::eMotionParamID_BlendWeight7, "BlendWeight7", "Custom 7");
}


SAdvancedAnimationComponent::~SAdvancedAnimationComponent()
{
	SAFE_RELEASE(m_pActionController);
}

void SAdvancedAnimationComponent::Initialize()
{
	LoadFromDisk();

	ResetCharacter();
}

void SAdvancedAnimationComponent::ProcessEvent(SEntityEvent& event)
{
	if (event.event == ENTITY_EVENT_UPDATE)
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];

		if (m_pActionController != nullptr)
		{
			m_pActionController->Update(pCtx->fFrameTime);
		}

		Matrix34 characterTransform = GetWorldTransformMatrix();

		// Set turn rate as the difference between previous and new entity rotation
		m_turnAngle = Ang3::CreateRadZ(characterTransform.GetColumn1(), m_prevForwardDir) / pCtx->fFrameTime;
		m_prevForwardDir = characterTransform.GetColumn1();

		if (m_pCachedCharacter != nullptr)
		{
			if (IPhysicalEntity* pPhysicalEntity = m_pEntity->GetPhysicalEntity())
			{
				pe_status_dynamics dynStatus;
				if (pPhysicalEntity->GetStatus(&dynStatus))
				{
					float travelAngle = Ang3::CreateRadZ(characterTransform.GetColumn1(), dynStatus.v.GetNormalized());
					float travelSpeed = dynStatus.v.GetLength2D();
					// Set the travel speed based on the physics velocity magnitude
					// Keep in mind that the maximum number for motion parameters is 10.
					// If your velocity can reach a magnitude higher than this, divide by the maximum theoretical account and work with a 0 - 1 ratio.
					if (!m_overriddenMotionParams.test(eMotionParamID_TravelSpeed))
					{
						m_pCachedCharacter->GetISkeletonAnim()->SetDesiredMotionParam(eMotionParamID_TravelSpeed, travelSpeed, 0.f);
					}

					// Update the turn speed in CryAnimation, note that the maximum motion parameter (10) applies here too.
					if (!m_overriddenMotionParams.test(eMotionParamID_TurnAngle))
					{
						m_pCachedCharacter->GetISkeletonAnim()->SetDesiredMotionParam(eMotionParamID_TurnAngle, m_turnAngle, 0.f);
					}

					if (!m_overriddenMotionParams.test(eMotionParamID_TravelAngle))
					{
						m_pCachedCharacter->GetISkeletonAnim()->SetDesiredMotionParam(eMotionParamID_TravelAngle, travelAngle, 0.f);
					}
				}
			}

			if (m_pPoseAligner != nullptr && m_pPoseAligner->Initialize(*m_pEntity, m_pCachedCharacter))
			{
				m_pPoseAligner->SetBlendWeight(1.f);
				m_pPoseAligner->Update(m_pCachedCharacter, QuatT(characterTransform), pCtx->fFrameTime);
			}
		}

		m_overriddenMotionParams.reset();
	}
	else if (event.event == ENTITY_EVENT_ANIM_EVENT)
	{
		if (m_pActionController != nullptr)
		{
			const AnimEventInstance *pAnimEvent = reinterpret_cast<const AnimEventInstance*>(event.nParam[0]);
			ICharacterInstance *pCharacter = reinterpret_cast<ICharacterInstance*>(event.nParam[1]);

			m_pActionController->OnAnimationEvent(pCharacter, *pAnimEvent);
		}
	}
	else if (event.event == ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED)
	{
		LoadFromDisk();
		ResetCharacter();
	}

	CBaseMeshComponent::ProcessEvent(event);
}

uint64 SAdvancedAnimationComponent::GetEventMask() const
{
	uint64 bitFlags = CBaseMeshComponent::GetEventMask() | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);

	if (m_pPoseAligner != nullptr)
	{
		bitFlags |= BIT64(ENTITY_EVENT_UPDATE);
	}

	if (m_pActionController != nullptr)
	{
		bitFlags |= BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_ANIM_EVENT);
	}

	return bitFlags;
}

void SAdvancedAnimationComponent::SetCharacterFile(const char* szPath)
{
	m_characterFile = szPath;
}

void SAdvancedAnimationComponent::SetMannequinAnimationDatabaseFile(const char* szPath)
{
	m_databasePath = szPath;
}

void SAdvancedAnimationComponent::SetControllerDefinitionFile(const char* szPath)
{
	m_defaultScopeSettings.m_controllerDefinitionPath = szPath;
}

void SAdvancedAnimationComponent::SetDefaultScopeContextName(const char* szName)
{
	m_defaultScopeSettings.m_contextName = szName;
}

void SAdvancedAnimationComponent::SetDefaultFragmentName(const char* szName)
{
	m_defaultScopeSettings.m_fragmentName = szName;
}