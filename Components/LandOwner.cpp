#include "LandOwner.h"
#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "BuildingComponent.h"
#include "CitizenComponent.h"
#include "StorageBuilding.h"

static void RegisterLandOwnerComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CLandOwnerComponent));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterLandOwnerComponent)

void CLandOwnerComponent::Initialize()
{
	CGamePlugin::AddLandOwner(this);
}

uint64 CLandOwnerComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE);
}

void CLandOwnerComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];
		Update(pCtx->fFrameTime);
	}
	break;
	}
}

void CLandOwnerComponent::ReflectType(Schematyc::CTypeDesc<CLandOwnerComponent>& desc)
{
	desc.SetGUID("{ED5CB331-7A56-4231-96AB-CFC4565E0BE6}"_cry_guid);
	desc.SetEditorCategory("Game");
	desc.SetLabel("Land Owner Component");
	desc.SetDescription("Land owner component attached to player or ai player handles land owning logic");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	//editor properties
	desc.AddMember(&CLandOwnerComponent::team, 'otea', "Team", "Team", "Team which this land owner belongs to", 0);
}

void CLandOwnerComponent::Update(float frameTime)
{
}

void CLandOwnerComponent::BearTheCosts(int cost)
{
	const int startCost = cost;
	for each(IUnit *pUnit in pUnits)
	{
		if (CStorageBuilding *pStorage = pUnit->GetEntity()->GetComponent<CStorageBuilding>())
		{
			if (pStorage->IsReady())
			{
				cost -= pStorage->GetStorage()[(int)EResourceTypes::RES_Gold];

				if (cost <= 0)
				{
					pStorage->RemoveResource((int)EResourceTypes::RES_Gold, startCost);
					return;
				}
				else
				{
					pStorage->SetResource((int)EResourceTypes::RES_Gold, 0);
				}
			}
		}
	}
}

int CLandOwnerComponent::GetGoldInAllGranaries()
{
	int entireGold = 0;
	for each(IUnit *pUnit in pUnits)
	{
		if (CStorageBuilding *pStorage = pUnit->GetEntity()->GetComponent<CStorageBuilding>())
		{
			if (pStorage->IsReady())
			{
				entireGold += pStorage->GetStorage()[(int)EResourceTypes::RES_Gold];
			}
		}
	}
	return entireGold;
}

IUnit *CLandOwnerComponent::CreateUnit(string unitName)
{
	//only apropiate for the unit maker buildings (like school or barracks)
	if (!pSelectedUnit)
		return nullptr; 

	if (SBuildingComponent *pUnitCreatorBuilding = pSelectedUnit->GetEntity()->GetComponent<SBuildingComponent>())
	{
		if (pUnitCreatorBuilding->IsUnitCreator())
		{
			return pUnitCreatorBuilding->CreateUnit(unitName);
		}
	}
	return nullptr;
}

SBuildingComponent * CLandOwnerComponent::CreateBuilding(string buildingName)
{
	SBuildingComponent *pReturnBuilding = nullptr;
	if (!pCreatingBuilding)
	{
		buildingName = buildingName.MakeLower();
		const string className = "schematyc::schematycentities::" + buildingName;
		IEntityClass *pEntityClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(className);
		if (pEntityClass)
		{
			SEntitySpawnParams spawn;
			spawn.pClass = pEntityClass;
			if (IEntity *pUnitEntity = gEnv->pEntitySystem->SpawnEntity(spawn))
			{
				if (pReturnBuilding = pUnitEntity->GetComponent<SBuildingComponent>())
				{
					int entireGold = 0;
					bool bRmv = true;
					for each(IUnit *pUnit in pUnits)
					{
						if (CStorageBuilding *pStorage = pUnit->GetEntity()->GetComponent<CStorageBuilding>())
						{
							entireGold += pStorage->GetStorage()[(int)EResourceTypes::RES_Gold];

							if (pReturnBuilding->GetCost() <= entireGold)
							{
								pReturnBuilding->SetOwner(this);
								pReturnBuilding->SetTeam(GetTeam());
								pCreatingBuilding = pReturnBuilding;
								pReturnBuilding = pReturnBuilding;
								bRmv = false;
								break;
							}
						}
					}
					if (bRmv)
					{
						gEnv->pEntitySystem->RemoveEntity(pUnitEntity->GetId());
						CryLogAlways("Not enough gold to create this building!");
					}
				}
			}
		}
	}
	else
	{
		gEnv->pEntitySystem->RemoveEntity(pCreatingBuilding->GetEntityId());
		pCreatingBuilding = nullptr;
	}
	return pReturnBuilding;
}

SBuildingComponent *CLandOwnerComponent::Build()
{
	if (pCreatingBuilding)
	{
		if (pCreatingBuilding->Build())
		{
			SBuildingComponent *pSavedCreatedBuilding = pCreatingBuilding;
			pCreatingBuilding = nullptr;
			AddUnit(pSavedCreatedBuilding);
			pSavedCreatedBuilding->SetTeam(GetTeam());
			return pSavedCreatedBuilding;
		}
	}
	return nullptr;
}

SBuildingComponent * CLandOwnerComponent::CreateAndBuildAtPoint(string buildingName, const Vec3 pos)
{
	SBuildingComponent *pReturnBuilding = nullptr;
	if (!pCreatingBuilding)
	{
		buildingName = buildingName.MakeLower();
		const string className = "schematyc::schematycentities::" + buildingName;
		IEntityClass *pEntityClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(className);
		if (pEntityClass)
		{
			SEntitySpawnParams spawn;
			spawn.pClass = pEntityClass;
			if (IEntity *pUnitEntity = gEnv->pEntitySystem->SpawnEntity(spawn))
			{
				if (pReturnBuilding = pUnitEntity->GetComponent<SBuildingComponent>())
				{
					int entireGold = 0;
					bool bRmv = true;
					for each(IUnit *pUnit in pUnits)
					{
						if (CStorageBuilding *pStorage = pUnit->GetEntity()->GetComponent<CStorageBuilding>())
						{
							entireGold += pStorage->GetStorage()[(int)EResourceTypes::RES_Gold];

							if (pReturnBuilding->GetCost() <= entireGold)
							{
								pReturnBuilding->SetOwner(this);
								pCreatingBuilding = pReturnBuilding;
								pReturnBuilding = pReturnBuilding;

								pUnitEntity->SetPos(pos);

								if (Build())
								{
									bRmv = false;
								}
								break;
							}
						}
					}
					if (bRmv)
					{
						gEnv->pEntitySystem->RemoveEntity(pUnitEntity->GetId());
						CryLogAlways("Not enough gold to create this building!");
						pReturnBuilding = nullptr;
						pCreatingBuilding = nullptr;
					}
				}
			}
		}
	}
	return pReturnBuilding;
}

void CLandOwnerComponent::SelectUnit(IUnit * pUnit)
{
	if (!pUnit)
	{
		if (pSelectedUnit)
		{
			pSelectedUnit->Deselect();
			pSelectedUnit = nullptr;
		}
	}
	else
	{
		if (pSelectedUnit)
		{
			if (pUnit != pSelectedUnit)
			{
				pSelectedUnit->Deselect();
				pSelectedUnit = pUnit;
				pSelectedUnit->Select();
			}
		}
		else
		{
			pSelectedUnit = pUnit;
			pSelectedUnit->Select();
		}
	}
}

void CLandOwnerComponent::GroupSelectUnit(IUnit * pUnit)
{
	if (!pUnit)
		return;

	pUnitsSelection.push_back(pUnit);
	pUnit->Select();
}

void CLandOwnerComponent::GroupDeselectUnit(IUnit * pUnit)
{
	if (!pUnit)
		return;

	pUnitsSelection.erase(std::remove(pUnitsSelection.begin(), pUnitsSelection.end(), pUnit), pUnitsSelection.end());
	pUnit->Deselect();
}

void CLandOwnerComponent::ClearGroupSelection()
{
	for each (IUnit *unt in pUnitsSelection)
	{
		unt->Deselect();
	}
	pUnitsSelection.clear();
}

void CLandOwnerComponent::AddUnit(IUnit * pUnit)
{
	if (!pUnit)
		return;

	pUnits.push_back(pUnit);
}

void CLandOwnerComponent::RemoveUnit(IUnit * pUnit)
{
	if (!pUnit)
		return;

	pUnits.erase(std::remove(pUnits.begin(), pUnits.end(), pUnit), pUnits.end());
}

std::vector<SBuildingComponent*> CLandOwnerComponent::GetBuildings()
{
	std::vector<SBuildingComponent*> pBuildings;
	for each(SBuildingComponent *pBuilding in pUnits)
	{
		pBuildings.push_back(pBuilding);
	}
	return pBuildings;
}
