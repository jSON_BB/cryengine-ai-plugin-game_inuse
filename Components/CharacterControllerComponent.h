#pragma once
#include "CryEntitySystem/IEntityComponent.h"
#include "CrySchematyc/Env/IEnvRegistrar.h"
#include "CrySchematyc/MathTypes.h"
#include "CryPhysics/physinterface.h"

struct SPhysics
{
	inline bool operator==(const SPhysics &rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }

	static void ReflectType(Schematyc::CTypeDesc<SPhysics>& desc)
	{
		desc.SetGUID("{CFF19D62-8DBD-4BE4-ADFD-2377A0DAF665}"_cry_guid);
		desc.AddMember(&SPhysics::m_mass, 'mass', "Mass", "Mass", "Mass of the character in kg", 80.f);
		desc.AddMember(&SPhysics::m_radius, 'radi', "Radius", "Collider Radius", "Radius of the capsule or cylinder", 0.45f);
		desc.AddMember(&SPhysics::m_height, 'heig', "Height", "Collider Height", "Height of the capsule or cylinder", 0.935f);
		desc.AddMember(&SPhysics::m_bCapsule, 'caps', "Capsule", "Use Capsule", "Whether or not to use a capsule as the main collider, otherwise cylinder", true);

		desc.AddMember(&SPhysics::m_bSendCollisionSignal, 'send', "SendCollisionSignal", "Send Collision Signal", "Whether or not this component should listen for collisions and report them", false);
	}

	Schematyc::PositiveFloat m_mass = 80.f;
	float m_radius = 0.45f;
	float m_height = 0.935f;
	bool m_bCapsule = true;

	bool m_bSendCollisionSignal = false;
};

struct SMovement
{
	inline bool operator==(const SMovement &rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }

	static void ReflectType(Schematyc::CTypeDesc<SMovement>& desc)
	{
		desc.SetGUID("{1D7CDACF-BFB9-479A-A88B-111ACF5611A3}"_cry_guid);
		desc.AddMember(&SMovement::m_airControlRatio, 'airc', "AirControl", "Air Control Ratio", "Indicates how much the character can move in the air, 0 means no movement while 1 means full control.", 1.f);
		desc.AddMember(&SMovement::m_airResistance, 'airr', "AirResistance", "Air Resistance", nullptr, 0.2f);
		desc.AddMember(&SMovement::m_inertia, 'iner', "Inertia", "Inertia Coefficient", "More amount gives less inertia, 0 being none", 8.f);
		desc.AddMember(&SMovement::m_inertiaAcceleration, 'inea', "InertiaAcc", "Inertia Acceleration Coefficient", "More amount gives less inertia on acceleration, 0 being none", 8.f);
		desc.AddMember(&SMovement::m_maxClimbAngle, 'maxc', "MaxClimb", "Maximum Climb Angle", "Maximum angle the character can climb", 50.0_degrees);
		desc.AddMember(&SMovement::m_maxJumpAngle, 'maxj', "MaxJump", "Maximum Jump Angle", "Maximum angle the character can jump at", 50.0_degrees);
		desc.AddMember(&SMovement::m_minSlideAngle, 'mins', "MinSlide", "Minimum Angle For Slide", "Minimum angle before the player starts sliding", 70.0_degrees);
		desc.AddMember(&SMovement::m_minFallAngle, 'minf', "MinFall", "Minimum Angle For Fall", "Minimum angle before the character starts falling", 80.0_degrees);
		desc.AddMember(&SMovement::m_maxGroundVelocity, 'maxg', "MaxGroundVelocity", "Maximum Surface Velocity", "Maximum velocity of the surface the character is on before they are considered airborne and slide off", DEG2RAD(50.f));
	}

	Schematyc::Range<0, 1> m_airControlRatio = 0.f;
	Schematyc::Range<0, 10000> m_airResistance = 0.2f;
	Schematyc::Range<0, 10000> m_inertia = 8.f;
	Schematyc::Range<0, 10000> m_inertiaAcceleration = 8.f;

	CryTransform::CClampedAngle<0, 90> m_maxClimbAngle = 50.0_degrees;
	CryTransform::CClampedAngle<0, 90> m_maxJumpAngle = 50.0_degrees;
	CryTransform::CClampedAngle<0, 90> m_minFallAngle = 80.0_degrees;
	CryTransform::CClampedAngle<0, 90> m_minSlideAngle = 70.0_degrees;

	Schematyc::Range<0, 10000> m_maxGroundVelocity = 16.f;
};

struct SCharacterControllerComponent final: public IEntityComponent
{
public:
	SCharacterControllerComponent() = default;
	virtual ~SCharacterControllerComponent();

	virtual void Initialize();
	virtual void   ProcessEvent(SEntityEvent& event);
	virtual uint64 GetEventMask() const override;
	static void ReflectType(Schematyc::CTypeDesc<SCharacterControllerComponent>& desc)
	{
		desc.SetGUID("{0CC5823C-CF30-43FC-8822-2395F61FA978}"_cry_guid);
	}
	bool IsOnGround() const { return m_bOnGround; }
	const Schematyc::UnitLength<Vec3>& GetGroundNormal() const { return m_groundNormal; }

	virtual void AddVelocity(const Vec3& velocity)
	{
		if (IPhysicalEntity* pPhysicalEntity = m_pEntity->GetPhysicalEntity())
		{
			pe_action_move moveAction;
			moveAction.iJump = 2;
			moveAction.dir = velocity;
			pPhysicalEntity->Action(&moveAction);
		}
	}

	virtual void SetVelocity(const Vec3& velocity)
	{
		if (IPhysicalEntity* pPhysicalEntity = m_pEntity->GetPhysicalEntity())
		{
			pe_action_move moveAction;
			moveAction.iJump = 1;
			moveAction.dir = velocity;
			pPhysicalEntity->Action(&moveAction);
		}
	}

	const Vec3& GetVelocity() const { return m_velocity; }
	Vec3 GetMoveDirection() const { return m_velocity.GetNormalized(); }

	bool IsWalking() const { return m_velocity.GetLength2D() > 0.2f && m_bOnGround; }

	virtual void Physicalize()
	{
		SEntityPhysicalizeParams physParams;
		physParams.type = PE_LIVING;
		physParams.nSlot = GetOrMakeEntitySlotId();
		physParams.mass = m_physics.m_mass;
		pe_player_dimensions playerDimensions;
		playerDimensions.bUseCapsule = m_physics.m_bCapsule ? 1 : 0;
		playerDimensions.sizeCollider = Vec3(m_physics.m_radius * 0.5f, 1.f, m_physics.m_height * 0.5f);
		if (playerDimensions.bUseCapsule)
		{
			playerDimensions.sizeCollider.z *= 0.5f;
		}
		playerDimensions.heightPivot = 0.f;
		playerDimensions.heightCollider = m_pTransform != nullptr ? m_pTransform->GetTranslation().z : 0.f;
		playerDimensions.groundContactEps = 0.004f;
		physParams.pPlayerDimensions = &playerDimensions;
		pe_player_dynamics playerDynamics;
		playerDynamics.mass = physParams.mass;
		playerDynamics.kAirControl = m_movement.m_airControlRatio;
		playerDynamics.kAirResistance = m_movement.m_airResistance;
		playerDynamics.kInertia = m_movement.m_inertia;
		playerDynamics.kInertiaAccel = m_movement.m_inertiaAcceleration;
		playerDynamics.maxClimbAngle = m_movement.m_maxClimbAngle.ToDegrees();
		playerDynamics.maxJumpAngle = m_movement.m_maxJumpAngle.ToDegrees();
		playerDynamics.minFallAngle = m_movement.m_minFallAngle.ToDegrees();
		playerDynamics.minSlideAngle = m_movement.m_minSlideAngle.ToDegrees();
		playerDynamics.maxVelGround = m_movement.m_maxGroundVelocity;
		physParams.pPlayerDynamics = &playerDynamics;
		m_pEntity->Physicalize(physParams);
		m_pEntity->UpdateComponentEventMask(this);
	}

	virtual void Ragdollize()
	{
		SEntityPhysicalizeParams physParams;
		physParams.type = PE_ARTICULATED;
		physParams.mass = m_physics.m_mass;
		physParams.nSlot = GetEntitySlotId();
		physParams.bCopyJointVelocities = true;
		m_pEntity->Physicalize(physParams);
	}

	virtual SPhysics& GetPhysicsParameters() { return m_physics; }
	const SPhysics& GetPhysicsParameters() const { return m_physics; }
	virtual SMovement& GetMovementParameters() { return m_movement; }
	const SMovement& GetMovementParameters() const { return m_movement; }
	virtual void SetUp(SPhysics phys, SMovement move)
	{
		m_physics = phys;
		m_movement = move;
		Physicalize();
	}
protected:
	bool m_bNetworked = false;

	SPhysics m_physics;
	SMovement m_movement;

	bool m_bOnGround = false;
	Schematyc::UnitLength<Vec3> m_groundNormal = Vec3(0, 0, 1);

	Vec3 m_velocity = ZERO;
};